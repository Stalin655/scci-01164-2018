package com.example.ilesson8;

import retrofit2.Call;
import retrofit2.Retrofit;

public class PingRepository {
    import com.example.ilesson8  ServerStatus;
import retrofit2.Call;
import android.util.Log;

import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

    public class PingRepository {

        private IPingWService pingws;

        public PingRepository() {
            Retrofit repo = new Retrofit.Builder()
                    .baseUrl("https://hillcroftinsurance.com:8445/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            this.pingws = repo.create(IPingWService.class);

        }

        public void getServerStatus() {
            this.pingws.getStatus().enqueue(new Callback<ServerStatus>() {
                @Override
                public void onResponse(Call<ServerStatus> call, Response<ServerStatus> response) {
                    ServerStatus r = response.body();
                    Log.e("IPING", r.getGroup());
                }

                @Override
                public void onFailure(Call<ServerStatus> call, Throwable t) {

                }
            });
        }
    }



}
