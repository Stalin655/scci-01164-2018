package com.example.ilesson8;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import android.os.Bundle;
import android.view.MenuItem;


import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity3 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        BottomNavigationView navview = findViewById(R.id.bottom_nav);
        navview.setOnNavigationItemSelectedListener(navListener);

        getSupportFragmentManager() .beginTransaction().replace(R.id.fragment_container, new Homefrag()).commit();
    }

    private  BottomNavigationView.OnNavigationItemSelectedListener navListener = (MenuItem)-{
        Fragment myFrag = null;

        switch (MenuItem.getItemId()){
         case R.id.nav_settings:
            myFrag = new SettingsFrag();
            break;
         case R.id.nav_home:
            myFrag = new HomeFrag();
            break;
         case R.id.nav_profile:
            myFrag = new ProfileFrag();
            break;
         case R.id.nav_notifications:
            myFrag = new NotificationsFrag();
            break;
        }




          }  getSupportFragmentManager().beginTransaction().replace (R.id.fragment_container,myFrag).commit();

            return true;
    }
}
